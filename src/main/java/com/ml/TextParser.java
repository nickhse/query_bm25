package com.ml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TextParser {

    private Map<Integer, String> mapDocs;
    private Map<String, List<Integer>> indexPage;

    public Map<String, List<Integer>> parseText(String fileName) throws IOException {
        String text = "";
        ClassLoader classLoader = Main.class.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        mapDocs = new HashMap<Integer, String>();
        int sizeBlock = 200;
        int blockNumber = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int i = 0;
            int docIndex = 1;
            StringBuilder textBuilder = new StringBuilder();

            StringBuilder textBlock = new StringBuilder();
            while ((line = br.readLine()) != null) {
                if (blockNumber > 60)
                  break;

                textBuilder.append(line);
                if (i < sizeBlock) {
                    textBlock.append(line);
                    ++i;
                } else {
                    mapDocs.put(docIndex, textBlock.toString());
                    i = 0;
                    ++docIndex;
                    textBlock.setLength(0);
                    blockNumber++;
                }
            }
            text = textBuilder.toString();
        }
        indexPage = getCalculatePageIndex(text, mapDocs);

        return indexPage;

    }

    private Map<String, List<Integer>> getCalculatePageIndex(String text, Map<Integer, String> mapDocs) {
        Map<String, List<Integer>> indexPage = new HashMap<>();
        List<String> tokenList = Arrays.asList(text.split("\\s"));
        for (String token : tokenList) {
            for (Map.Entry<Integer, String> entry : mapDocs.entrySet()) {

                String blockText = entry.getValue();
                if (blockText.contains(token)) {
                    if (indexPage.get(token) != null) {
                        List<Integer> indexSet = indexPage.get(token);
                        if (!indexSet.contains(entry.getKey()))
                            indexSet.add(entry.getKey());
                    } else {
                        List<Integer> indexSet = new ArrayList<>();
                        indexSet.add(entry.getKey());
                        indexPage.put(token, indexSet);
                    }

                }
            }

        }
        return indexPage;
    }

    private void culculateDeltaInPageIndex(Map<String, List<Integer>> indexPage) {
        indexPage.entrySet().stream().forEach(entry -> {
            List<Integer> integers = entry.getValue();
            List<Integer> diff = new ArrayList<>();
            diff.add(integers.get(0));
            if (integers.size() != 1) {
                for (int i = 1; i < integers.size(); i++) {
                    Integer prev = integers.get(i - 1);
                    Integer current = integers.get(i);
                    Integer diffInteger = current - prev;
                    diff.add(diffInteger);
                }
                indexPage.put(entry.getKey(), diff);
            }
        });
    }

    public Map<Integer, String> getMapDocs() {
        return mapDocs;
    }

    public Map<String, List<Integer>> getIndexPage() {
        return indexPage;
    }
}
