package com.ml;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.function.BiFunction;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;

public class SyntaxTree {
    private final Map<String, BiFunction<Object, Object, Collection<Integer>>> operators;
    Map<String, List<Integer>> indexPage;

    public SyntaxTree(Map<String, List<Integer>> indexPage) {
        this.indexPage = indexPage;

        operators = new HashMap<>();
        operators.put("AND", (a, b) -> Sets.intersection(getDocuments(a), getDocuments(b)));
        operators.put("OR", (a, b) -> {

            Set<Integer> result1 = getDocuments(a);
            Set<Integer> result2 = getDocuments(b);
            if (CollectionUtils.isNotEmpty(result2))
                result1.addAll(result2);
            return result1;
        });
        operators.put("OR_NOT", (a, b) -> {
            Set<Integer> result1 = getDocuments(a);
            Set<Integer> result2 = getDocuments(b);
            result1.removeAll(result2);
            return result1;
        });


    }

    private Set<Integer> getDocuments(Object o) {
        if (o instanceof String)
            return newHashSet(indexPage.get(((String) o).trim()));
        else
            return (Set<Integer>) o;
    }

    public Collection<Integer> evaluate(String query) {
        Node tree = buildTree(query);
        return (Collection<Integer>) parseTree(tree);

    }

    private Object parseTree(Node node) {
        Node leftNode = node.left;
        Node rightNode = node.right;

        if (leftNode != null && rightNode != null) {
            BiFunction fn = operators.get(node.value);
            return fn.apply(parseTree(leftNode), parseTree(rightNode));
        } else {
            return node.value;
        }
    }

    private Node buildTree(String query) {

        String[] tokens = query.split("\\s+");
        Node currentRoot = new Node();
        Stack<Node> stack = new Stack();
        stack.push(currentRoot);
        for (String token : tokens) {

            if (token.equals("(")) {

                currentRoot.left = new Node();
                stack.push(currentRoot);
                currentRoot = currentRoot.left;
            } else if (!asList("OR", "OR_NOT", "AND", ")").contains(token)) {

                currentRoot.value = token;
                currentRoot = stack.pop();
            } else if (asList("OR", "OR_NOT", "AND").contains(token)) {

                currentRoot.value = token;
                currentRoot.right = new Node();
                stack.push(currentRoot);
                currentRoot = currentRoot.right;
            } else if (token.equals(")")) {
                currentRoot = stack.pop();

            } else {
                throw new RuntimeException("Upss....error");
            }

        }
        return currentRoot;
    }

    private class Node {
        String value;
        Node left;
        Node right;

        Node() {
            right = null;
            left = null;
        }
    }
}
