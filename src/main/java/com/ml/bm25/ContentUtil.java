package com.ml.bm25;

import java.util.Arrays;
import java.util.Collection;

public class ContentUtil {


    public static int countAvgDocLength(Collection<String> pages) {
        int numberWords = 0;
        int numberPages = pages.size();
        for (String page : pages) {
            numberWords += page.split("\\s+").length;
        }

        return numberWords / numberPages;
    }

    public static int countWords(String[] words, String[] targetWords) {
        int count = 0;
        for(String targetWord :targetWords) {
            for (String w : Arrays.asList(words)) {
                if (targetWord.toLowerCase().contains(w.toLowerCase())) {
                    count++;
                }
            }
        }
        return count;
    }


    public static int countWordInDocuments(String[] targetWords, Collection<String> pages) {
        int conut = 0;
        for (String targetWord : targetWords) {
            for (String page : pages) {
                if (page.contains(targetWord.toLowerCase())) {
                    conut++;
                }
            }
        }
        return conut;
    }
}
