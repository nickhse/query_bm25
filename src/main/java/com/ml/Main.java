package com.ml;

import com.ml.bm25.BM25;
import com.ml.bm25.ContentUtil;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.ml.bm25.ContentUtil.countAvgDocLength;
import static com.ml.bm25.ContentUtil.countWordInDocuments;
import static java.util.stream.Collectors.toMap;

public class Main {

    public static void main(String[] args) throws IOException {
        TextParser textParser = new TextParser();
        textParser.parseText("tom_1.txt");
        Map<String, List<Integer>> indexPage = textParser.getIndexPage();
        Map<Integer, String> mapDocs = textParser.getMapDocs();
        String[] targetWords = {"рассказ", "слишком", "конь"};
        SyntaxTree syntaxTree = new SyntaxTree(indexPage);

        Collection<Integer> documents = syntaxTree.evaluate("( ( рассказ AND конь ) OR  ( слишком AND конь ) )");


        Map<Integer, String> mapDocsFilter = mapDocs.entrySet().stream()
                .filter(map -> documents.contains(map.getKey()))
                .collect(toMap(p -> p.getKey(), p -> p.getValue()));

        int docNumber = documents.size();
        int pageNumberWithWord = countWordInDocuments(targetWords, mapDocsFilter.values());
        int avgDocLength = countAvgDocLength(mapDocsFilter.values());

        BM25 bm25 = new BM25();

        for (Map.Entry<Integer, String> entryPage : mapDocsFilter.entrySet()) {
            Integer docId = entryPage.getKey();
            String page = entryPage.getValue();

            String[] words = page.split("\\s+");

            int qtf = ContentUtil.countWords(words, targetWords);
            int docLength = words.length;


            float score = bm25.computeScore(1, qtf, docLength, avgDocLength, docNumber, pageNumberWithWord);

            System.out.println("Page score for docId " + docId + " is : " + score);
        }

    }

}
